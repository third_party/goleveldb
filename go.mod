module github.com/syndtr/goleveldb

go 1.16

require (
	github.com/golang/snappy v0.0.4
	github.com/onsi/ginkgo v1.7.0
	github.com/onsi/gomega v1.4.3
)
